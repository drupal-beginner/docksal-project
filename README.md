# Docksal powered Drupal Installation

This is the newest Drupal installation pre-configured for use with Docksal.

## Setup instructions

### Step #1: Docksal environment setup

**This is a one time setup - skip this if you already have a working Docksal environment.**

Follow [Docksal environment setup instructions](https://docs.docksal.io/getting-started/setup/)

### Step #2: Project setup

1. Clone this repo into your Projects directory. Where `drupal` (the third argument) name of your project, so change it if you need.

    ```
    git clone https://gitlab.com/drupal-beginner/docksal-project.git drupal
    cd drupal
    ```

2. Initialize the site

   This will install the newest Drupal Recommended project version via composer.

    ```
    fin init
    ```

3. Point your browser to

   Note that if you change the project's name (the third argument in the first step) you should change domain respectively

    ```
    http://drupal.docksal
    ```

When the automated install is complete the command line output will display link to your site.
